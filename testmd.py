import multid as md
import numpy as np
import random
import matplotlib.pyplot as plt 
def circle_sect (R0, t0, length):
    datax = [0]*length
    datay = [0]*length
    datax[0] = R0*np.cos(t0)
    datay[0] = R0*np.sin(t0)
    for t in range(1,length):
        if (t%2 == 1):
            datay[t] = datay[t - 1] + (1./20.)*datax[t - 1]
            datax[t] = datax[t - 1] + (-1./20.)*datay[t]
        else:
            datax[t] += datax[t - 1] + (-1./20.)*datay[t - 1]
            datay[t] += datay[t - 1] + (1./20.)*datax[t]
    # noise
        fact0 = np.exp(-((datay[t] - 0.5)**2 + datax[t]**2))
        #fact0 = 1
        for j in range(100):
            datay[t] += fact0*random.uniform (-1/100, 1/100.)
            datax[t] += fact0*random.uniform (-1/100, 1/100.)/2


    for i in range(len(datax)):
        if (random.uniform (0, 1) < 0.01):
            datax[i] = -9999
        if (random.uniform (0, 1) < 0.01):
            datay[i] = np.NaN
        if (random.uniform (0, 1) < 0.01):
            datax[i] = np.NaN
            datay[i] = -9999
    return datax, datay
          
def circle_data (sections, maxlen = 300):
    Data = []
    Dx = []
    Dy = []
    for i in range(sections):
        dx, dy = circle_sect (random.uniform(0,1), random.uniform(0, 13), random.randrange(int(maxlen/3),maxlen))
        Dx.append(dx)
        Dy.append(dy)
    Data.append(Dx)
    Data.append(Dy)
    return Data


def linear_data (sections, maxlen = 300):
    Data = []
    Dx = []
    Dy = []
    for i in range(sections):
        lent = random.randrange(0,maxlen)
        dx = [random.uniform(-4, 10)]*lent
        for i in range(len(dx)):
            dx[i] += random.uniform(-0.01, 0.01)
        strt = random.randrange(-30,30)
        dy = np.array(range(strt, strt + lent))
        Dx.append(dx)
        Dy.append(dy)
    Data.append(Dx)
    Data.append(Dy)
   
    return Data


def plot_D1_circle(level = 4):
    Data = circle_data (20,500)
    print (Data)
    D1, D2, H = md.multid3(Data, level = level, test = True)
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    U = D1[:,:,0]
    V = D1[:,:,1]
    m1 = np.mean(np.mean(D2[:,:,0]))
    m2 = np.mean(np.mean(D2[:,:,1]))

    print (f'ratio : {m1/m2}')
    plt.pcolor (yy,xx, D2[:,:,1], vmin = 0, vmax = 2*np.mean(np.mean(D2[:,:,0])))
    plt.quiver (yy, xx, U, V)
    plt.plot (Data[0][0][:], Data[1][0][:], color = 'magenta')
    plt.show()
    
def test_dep():
    Data = circle_data (5,500)
    print (len([Data[1]]))
    Av, Var, H = md.dependency_check (Data[0], [Data[1]], 5)
    print (H)
    print (Var)
