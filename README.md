# Ny-Alesund data analysis 

This document aims to provide an overview of the most relevant files
and folders that were used for data analysis on the alleged
cloud-system bi-stability in the Arctic region. Since there has been
some unfruitful analysis, you may find some files that are not listed
below.

## Libraries
(Click link to open)

* [Multi-dimensional Langevin analyis](multid.py)
* [Resample and filter time series data](time_align.py)
* [Principal Component analysis of ERA data](PCA_era.py)
* [Fokker-Plank equation solver](basislisk.fr/sandbox/Antoonvh/fpe.h)
* [Steady-State respecting Drift and diffusion](steady/steady.h)
* [Langevin-Equation solver for the toymodel](toymodel/le.c)

## Test

* [Langevin analysis of a cyclic system with noise](testmd.py)
* [Steady-state respecting Drift and Diffusion for a simple system](hist-fp.c)

## Usage

* [Apply the multidimensional analysis to data](usemd.py)
* [Stochastic prediction useing the Fokker-Plank equation](fpe.c)
* [Steady-state respecting coefficients for the Ny-Alesund data](steady/steady.c)

## Data

For our analysis we have used BSRN data, ERA data, and some Korean
database for the particle counter. Also local meteo was used and
cloudnet observations/products. These latter two do not appear in the
written analysis. But the other data is backed-up. Furthermore, it
comes with scripts that we use to obtain or modify the file format,
these are mostly written in the `fish`-shell language, which you may
need to install along with the core utils (sed, awk, cut`, etc.).

## Utilities

* [Lost? Search for a keyword in all `*.py` files](find_snippit.fish)
* [Read time & data files](data_reader)
* [Request data from the ERA server using the Python API](era_reques.py)
* [A (fish-) shell script to help call the aforementioned ERA-data request script](era_req.fish)

## Figures

The figures in the draft document are generated using the `plot.py`
scripts, except for the "location of the stable attractor"
plots. Furtheremore, the plotted data should be available in the
designated folder in the `./paper/figures` folder.

## Dependencies 

I have used Python3(.11) and some libraries as provided by my
operating system (Debian Bookworm, i.e. current testing) using
something like `$sudo apt install python3-[lib]`. Hoping that my
implementations are not too-much relying on edge case
functionality. You can view the list of my installed packages and
their version [`here`](pythonlibs.txt).

## Contact

All these codes were written by Antoon van Hooft for my research
project supervised by Franziska Glassmeier. If you have questions,
sorry for the mess, it was mostly written for "single use". You could
try and contact me, perhaps via e-mail: `j.a.v.hooft@gmail.com`
