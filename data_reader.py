import time_align as ta
from datetime import date, timedelta
import time as tm
import os.path
import netCDF4 as nc
import math
import numpy as np

def write_ascii (col1, col2, fname):
    fp = open (fname, "w+")
    for  i in range(len(col1)):
        fp.write (f"{col1[i]} \t {col2[i]}\n")
    fp.close()
    
def read_write_LWP ():
    sdate = date(2016,9,1)
    edate = date(2018,2,28)
    data = []
    for n in range(((edate - sdate).days)):
        s = str(sdate + timedelta(n))
        s = s.replace ('-', '') + '.nc'
        file = 'data/lwp/'+ s
        if os.path.isfile(file):
            ds=nc.Dataset(file)
            _time = ds['time'][:]
            time_temp = []
            lwp = ds['lwp'][:]
            #LWP = np.concatenate ((LWP, lwp))
            for i in range(len(_time)):
                temp = ta.stamp_from_dat_hours (sdate+timedelta(n), _time[i])
                if float(lwp[i]) > -1:
                    data.append ([temp, lwp[i]])
                    
        else:
            print ('not found: ' + file)

    time = [0]*len(data)
    LWP = [0]*len(data)
    for i in range(len(data)):
        time[i] = data[i][0]
        LWP[i] = data[i][1]
                
                             
                             
    write_ascii (time, LWP, 'LWP.txt')
    return LWP, time



def read_write_N ():
    file = '/home/antoon/nya/data/N/N.nc'
    if (os.path.isfile(file)):
        string = str(2015) + ' ' + str(11) + ' ' + str(27) + ' ' + str(8) + ' ' + str (11) + ' ' + str(25)
        offset = tm.mktime(tm.strptime(string, "%Y %m %d %H %M %S"))
        ds = nc.Dataset(file);
        time = ds['time'][:]
        N = ds['total_aerosol_particle_concentration'][:]
        for i in range(len(time)):
            time[i] = time[i] + offset

    write_ascii (time, N, 'N.txt')
    return N, time


def realistic_LW (val):
    if (val > 100 and val < 400):
        return True
    return False
 


def read_BSRN(startyear, endyear):
    path = 'data/BSRN/datasets/'
    year = startyear
    data = []
    ind = 0
    indprev = 0
    while (year <= endyear):
        for i in [1,2,11,12]:
            #i = i + 1
            if (i < 10):
                file = path + str(year) + '-0' + str(i)
            else:
                file = path + str(year) + '-' + str(i)
            
            print (file)
            if os.path.isfile(file):
                print (file)
                FILE = open(file, 'r')
                print (FILE)
                for row in open(file, 'r'):
                    a = row.split()
                    stri = a[0].replace('-', ' ')
                    stri += ' ' + a[1].replace(':', ' ')
                    A = tm.strptime(stri, "%Y %m %d %H %M")
                    timestmp = tm.mktime(A)
                    a1 = [timestmp, float(a[2])/float(a[3])]
                    if (not realistic_LW(float(a[2]))):
                        print ("erreur")
                        print (file)
                        print (stri)
                        return 1;
                    data.append(a1)
                    ind = ind + 1
                    if (ind > indprev + 10000):
                        indprev = ind
                        print (ind)
                    
            else :
                print ('Could not find: ' + file)
        year = year + 1
    times = [0]*len(data)
    rat = [0]*len(data)
    for i in (range(len(data))):
        times[i] = data[i][0]
        rat[i] = data[i][1]
             
    
    write_ascii (times, rat, f'Rrat{startyear}_{endyear}.txt')
    return data


def read_BSRN_diff(startyear, endyear):
    path = 'data/BSRN/datasets/'
    year = startyear
    data = []
    ind = 0
    indprev = 0
    while (year <= endyear):
        for i in [1,2,11,12]:
            #i = i + 1
            if (i < 10):
                file = path + str(year) + '-0' + str(i)
            else:
                file = path + str(year) + '-' + str(i)
            print (file)
            if os.path.isfile(file):
                print (file)
                FILE = open(file, 'r')
                print (FILE)
                for row in open(file, 'r'):
                    a = row.split()
                    stri = a[0].replace('-', ' ')
                    stri += ' ' + a[1].replace(':', ' ')
                    A = tm.strptime(stri, "%Y %m %d %H %M")
                    timestmp = tm.mktime(A)
                    a1 = [timestmp, (float(a[2]) - float(a[3]))/float(a[3])]
                    if (not realistic_LW(float(a[2]))):
                        print ("erreur")
                        print (file)
                        print (stri)
                        return 1;
                    data.append(a1)
                    ind = ind + 1
                    if (ind > indprev + 10000):
                        indprev = ind
                        print (ind)
                    
            else :
                print ('Could not find: ' + file)
        year = year + 1
    times = [0]*len(data)
    rat = [0]*len(data)
    for i in (range(len(data))):
        times[i] = data[i][0]
        rat[i] = data[i][1]
             
    
    write_ascii (times, rat, f'Rdifnorm{startyear}_{endyear}.txt')
    return data

