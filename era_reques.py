#!/usr/bin/env python
#'variable': [
#                   'specific_humidity', 'temperature', 'u_component_of_wind',
#                   'v_component_of_wind', 'vertical_velocity',
#
import cdsapi
import sys
c = cdsapi.Client()

if (len(sys.argv) == 2):
    year = int(sys.argv[1])

if year < 1960 or year > 2030:
    print ('Wrong year!')
    print (year)
    sys.exit
    

c.retrieve('reanalysis-era5-pressure-levels',
           {
               'product_type': 'reanalysis',
               'variable': [
                   'potential_vorticity'
               ],
               'pressure_level': '900',
               'year': [
                   f'{year}',
               ],
               'month': [
                   '01', '02', '11',
                   '12',
               ],
               'day': [
                   '01', '02', '03',
                   '04', '05', '06',
                   '07', '08', '09',
                   '10', '11', '12',
                   '13', '14', '15',
                   '16', '17', '18',
                   '19', '20', '21',
                   '22', '23', '24',
                   '25', '26', '27',
                   '28', '29', '30',
                   '31',
               ],
               'time': [
                   '00:00', '01:00', '02:00',
                   '03:00', '04:00', '05:00',
                   '06:00', '07:00', '08:00',
                   '09:00', '10:00', '11:00',
                   '12:00', '13:00', '14:00',
                   '15:00', '16:00', '17:00',
                   '18:00', '19:00', '20:00',
                   '21:00', '22:00', '23:00',
               ],
               'format': 'netcdf',
           },
           f'era_potvort_{year}.nc')
print (f"Year '{year} Downloaded")
    
    
