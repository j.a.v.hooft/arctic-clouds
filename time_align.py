import numpy as np
from datetime import date, timedelta
import time as tm
import os.path
import netCDF4 as nc
import math
import csv

def stamp_from_dat_hours (dat, hours):
    string = str(dat.year) + ' ' + str(dat.month) + ' ' + str(dat.day)
    hour = math.floor(hours)
    mins = 60*(hours - hour)
    min = math.floor (mins)
    secs = 60*(mins - min)
    sec = math.floor(secs)
    string += ' ' + str(hour) + ' ' + str(min) + ' ' + str(sec)
    A = tm.strptime(string, "%Y %m %d %H %M %S")
    return tm.mktime(A)

def times_from_dat_hours (dats, hours):
    time = [0 for i in range(len(dats))]
    for i in range(len(time)):
        time[i] = stamp_from_dat_hours (dats[i], hours[i])
    return time

import hist
# Provided as decimal hours starting from 00:00:00 (UTC) every day
def times_for_LWP ():
    sdate = date(2016,11,1)
    edate = date(2018,2,28)
    time = []
    for n in range(((edate - sdate).days)):
        s = str(sdate + timedelta(n))
        s = s.replace ('-', '') + '.nc'
        file = 'data/lwp/'+ s
        if os.path.isfile(file):
            ds=nc.Dataset(file);
            _time = ds['time'][:]
            for i in range(len(_time)):
                _time[i] = stamp_from_dat_hours (sdate+timedelta(n), _time[i])
            time = np.concatenate ((time, _time))
        else:
            print ('not found: ' + file)
    return time        

def hours_from_mins_secs (mins, secs = 0):
    return mins/60 + secs/3600

def hours_from_hours_mins (hoursmins):
    hours = [0] * len(hoursmins)
    i = 0
    for tim in hoursmins:
        hour, mins = tim.split(':')
        hours[i] = float(hour) + float(mins)/60.
        i += 1
    return hours

def dates_from_string (data):
    
    return dates

# Provided as time since 2015-11-27 8:23:25 UTC `proleptic gregorian`
def times_for_N ():
    file = '/home/antoon/nya/data/N/N.nc'
    if (os.path.isfile(file)):
        string = str(2015) + ' ' + str(11) + ' ' + str(27) + ' ' + str(8) + ' ' + str (11) + ' ' + str(25)
        offset = tm.mktime(tm.strptime(string, "%Y %m %d %H %M %S"))
        ds = nc.Dataset(file);
        time = ds['time'][:]

        for i in range(len(time)):
            time[i] = time[i] + offset
    return time

#Unsure about the timing. Assuming UTC
def times_for_meteo(time, data):
    times = [0] * len(time)
    for i in range(len(time)):
        str = data[i].replace('-', ' ')
        str += ' ' + time[i].replace(':', ' ')
        A = tm.strptime(str, "%Y %m %d %H %M")
        times[i] = tm.mktime(A)
    return times

def create_time_array (tstart, tend, dt):
    return np.arange (tstart, tend, dt)

def realistic_LWP(LWP):
    if LWP >= 0 and LWP < 3:
        return True
    return False

def realistic_meteo(val):
    if (val >= -100 and val < 400):
        return True
    if (val >= 800 and val < 1200):
        return True
    return False

def realistic_rat(val):
    if val > 0 and val < 2 :
        return True
    return False

def homogenize_gaussian (time, dt, timed, data, realistic, fw = 0, threshold = 0.01):
    if fw == 0:
        fw = dt
    fillval = -9999
    dpmin = 1
    jstart = 1
    dtsq = fw*fw
    datanew = [0 for i in range(len(time))]
    for i in range(len(time)):
        weight = 0
        tot = 0
        timei = time[i]
        ## Goto starting point in the original data array
        j = jstart
        while ((np.exp(-(((timed[j] - timei)**2)/dtsq)) < threshold) and
               timed[j] < timei):
            j += 1
        if j > 0:
            j - 1 # Do-while structure
        jstart = j
        ## Attempt Gaussian averaging require `dpmin` datapoints, some before and after timei
        before, after = False, False
        dp = 0 # data points
        
        while np.exp(-(((timed[j] - timei)**2)/dtsq)) > threshold:
            if realistic(data[j]):
                weight += np.exp(-(((timed[j] - timei)**2)/dtsq))
                tot += data[j]*np.exp(-(((timed[j] - timei)**2)/dtsq))
                dp += 1
                if timed[j] < timei:
                    before = True
                if timed[j] > timei:
                    after = True
            j += 1
        if weight > 0 and dp >= dpmin and before and after:
            datanew[i] = tot/weight
        else:
            datanew[i] = fillval
    return datanew



def read_time_data(filename):
    num_lines = sum(1 for line in open(filename))
    time, LWP = [], []
    time = [0 for i in range(num_lines)]
    LWP = [0 for i in range(num_lines)]
    i = 0
    with open(filename, newline = "\n") as datas:
        datum = csv.reader(datas, delimiter = '\t')
        for a in datum:
            time[i] = float(a[0])
            try:
                LWP[i] = float(a[1])
            except:
                LWP[i] = -999
            i += 1
    return time, LWP
         

def realistic_N (N):
    if (N>=0 and N <=5000):
        return True
    return False

def homogenize_LWP_N ():
    timeN, N = read_time_data ('N.txt')
    timeLWP, LWP = read_time_data ('LWP.txt')
    tstart = max(timeN[1], timeLWP[1])
    tstop = min (max(timeLWP), max(timeN)) - 10000
    dt = 600
    time = create_time_array (tstart, tstop, dt)
    hN = homogenize_gaussian (time, dt, timeN, N, realistic_N)
    hLWP = homogenize_gaussian (time, dt, timeLWP, LWP, realistic_LWP)
    return hN, hLWP



def homogenize_data ():
    timeN, N = read_time_data ('N.txt')
    timeLWP, LWP = read_time_data ('LWP.txt')
    timer, data, Pres, Temp, Wind, RH, Wdir = nya.read_meteo()
    timeMET = ta.times_for_meteo (timer, data)
    tstart = max(timeN[1], timeLWP[1])
    tstart = max (tstart, timeMET[1]) + 1000
    tstop = min (max(timeLWP), max(timeN))
    tstop = min (tstop, max(timeMET))
    dt = 600
    time = create_time_array (tstart, tstop, dt)
    hN = homogenize_gaussian (time, dt, timeN, N, realistic_N)
    hLWP = homogenize_gaussian (time, dt, timeLWP, LWP, realistic_LWP)
    Presh = homogenize_gaussian (timeh, dt, times, Pres, ta.realistic_meteo)
    Temph = homogenize_gaussian (timeh, dt, times, Temp, ta.realistic_meteo)
    Windh = homogenize_gaussian (timeh, dt, times, Wind, ta.realistic_meteo)
    RHh   = homogenize_gaussian (timeh, dt, times, RH  , ta.realistic_meteo)
    Wdirh = homogenize_gaussian (timeh, dt, times, Wdir, ta.realistic_meteo)

    

         
         
        
         
        
         
    
    
