import multid as md
import time_align as ta
import numpy as np
import matplotlib.pyplot as plt
import data_reader as dr

def plit_data (time, data):
    datan = []
    data = np.array(data)
    datan.append (list(data[0:int(len(data)/2)]))
    datan.append (list(data[int(len(data)/2) + 1:len(data) - 1 ]))
    print (len(datan))
    return datan

def weigted_mean (D, N):
    a = D.shape
    S = a[0]
    dims = a[-1]
    cells = S**dims
    means = [0]*dims
    tots = [0]*dims
    
    total = np.sum(N)
    mpc = total / (cells)
    print (f"mpc = {mpc}")
    R = [1]
    for i in range(dims - 1):
        R.append (pow(S, i + 1))
    for cell in range(cells): 
        i = md.cell_index (cell, R)
        for dim in range(dims):
            il = list(i)
            il.append(dim)
            ine = tuple(i)
            if (N[i] > mpc): # Significant cell
                means[dim] += D[ine]*N[i]
                tots[dim] += N[i]
    for dim in range(dims):
        if (tots[dim] > 0):
            means[dim] /= tots[dim]
    
    return means

def usemd_on_files(files, level = 4, test = False) :
    single = False
    if str(type(files)) == "<class 'str'>": # Convert to list
        single = True
        files = [files]
    elif len(files) == 1:
        single = True
    data = []
    for f in files:
        time, dat = ta.read_time_data (f)
        dat = [dat]
        data.append(dat)
    D1, D2, H = md.multid3(data, dt = 600, level = level, test = test)
    meanm = weigted_mean (D2, H)
    
    #if single:
    #    maxm = np.max(D2)
    #elif  not single:
    #    maxm = np.max(D2[:,:,0])
    print (f'Mean diff: {meanm[0]}')
    return D1, D2, H

def RAT_RH(level = 4):
    time, RH = ta.read_time_data ("RHh.txt")
    time, Rrath = ta.read_time_data ("Rrath.txt")
    RHs = [RH]
    rat = [Rrath]
    Data = [RHs,rat]
  
    D1, D2, H = md.multid3(Data, dt = 600, level = level, test = True)
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    for i in range(len(D1)):
        for j in range(len(D1[i])):
            if (H[i,j] < 50):
                for k in range(len(D1[i,j])):
                    D1[i,j,k] = 0
                    D2[i,j,k] = 0
    if 0:
        fig, (ax1) = plt.subplots(1, 1)
        ax1.pcolor (yy,xx, H, vmin = 0, vmax = 2*np.mean(np.mean(H)))
        U = D2[:,:,0]
        V = np.dot(U, 0)
        #plt.hist2d (Data[0][0][:], Data[1][0][:], Rrath, range = [[40,100],[0.65,1.05]])
        ax1.quiver (yy, xx, U, V, color = 'magenta')
        U = np.dot(U,-1)
        ax1.quiver (yy, xx, U, V, color = 'magenta')
        V = D2[:,:,1]
        U = np.dot(U, 0)
        ax1.quiver (yy, xx, U, V, color = 'magenta')
        V = np.dot(V,-1)
        ax1.quiver (yy, xx, U, V, color = 'magenta')
        U = D1[:,:,0]
        V = D1[:,:,1]
        ax1.quiver (yy,xx, U, V)
        ax1.set_xlabel ('RH [norm.]')
        ax1.set_ylabel ('LWD/WLU [norm.]')
        ax1.set_title ('2D Flow and variation + histogram')
        
        ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
        plt.savefig ("var_RH_rat.png", dpi = 300)
        plt.cla()
    T, Pe = md.residence_time (D1, D2, test = True)
   # return T, Pe, D1, D2
    fig, (ax1, ax2) = plt.subplots(1, 2)
    plt.title ('Statistics for LWD/WLU')
    Trat = np.dot(T[:,:,1], 1./3600.)
    a = ax1.pcolor (yy,xx,Trat, vmin = 0, vmax = 13)
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Residence time [hours]')
    Perat = Pe[:,:,1]
    a = ax2.pcolor (yy,xx, Perat, vmin = 0, vmax = 1)
    
    ax2.set_aspect(1.0/ax2.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax2, location = 'bottom')
    b.set_label ('Diffusion-advection ratio')
    plt.savefig('time_rat.png', dpi = 200)
    plt.cla()
    
    fig, (ax1, ax2) = plt.subplots(1, 2)
    plt.title ('Statistics for RH')
    Trat = np.dot(T[:,:,0], 1./3600.)
    a = ax1.pcolor (yy,xx,Trat, vmin = 0, vmax = 13)
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    ax1.set_xlabel ('RH [norm.]')
    ax1.set_ylabel ('LWD/WLU [norm.]')
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Residence time [hours]')
    Perat = Pe[:,:,0]
    a = ax2.pcolor (yy,xx, Perat, vmin = 0, vmax = 1)
    ax2.set_xlabel ('RH [norm.]')
    ax2.set_ylabel ('LWD/WLU [norm.]')
 
    ax2.set_aspect(1.0/ax2.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax2, location = 'bottom')
    b.set_label ('Diffusion-advection ratio')
    plt.savefig ('time_RH.png', dpi = 200)
    

def PRES_RH(level = 4):
    time, RH = ta.read_time_data ("RHh.txt")
    time, presh = ta.read_time_data ("Presh.txt")
    RHs = plit_data (time, RH)
    pres = plit_data (time, presh)
    Data = []
    
    Data.append(RHs)
    Data.append(pres)
    #Data = md.Normalize_data(Data)
    #print (Data)
    #return Data
  
    D1, D2, H = md.multid3(Data, level = level, test = True)
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    for i in range(len(D1)):
        for j in range(len(D1[i])):
            if (H[i,j] < 50):
                for k in range(len(D1[i,j])):
                    D1[i,j,k] = 0
                    D2[i,j,k] = 0
    fig, (ax1) = plt.subplots(1, 1)

    ax1.pcolor (yy,xx, H, vmin = 0, vmax = 2*np.mean(np.mean(H)))
    U = D1[:,:,0]
    V = D1[:,:,1]
    #plt.hist2d (Data[0][0][:], Data[1][0][:], Rrath, range = [[40,100],[0.65,1.05]])
    ax1.quiver (yy, xx, U, V)
    ax1.set_xlabel ('RH [norm.]')
    ax1.set_ylabel ('Pres [norm.]')
    ax1.set_title ('2D flow and histogram')
    
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    #print (Data[0][0][:])
    #print (Data[1][0][:])
    #plt.savefig ("flow_RH_rat.png", dpi = 300)
    #plt.show()
    plt.cla()

    fig, (ax1) = plt.subplots(1, 1)
    ax1.pcolor (yy,xx, H, vmin = 0, vmax = 2*np.mean(np.mean(H)))
    U = D2[:,:,0]
    V = np.dot(U, 0)
    #plt.hist2d (Data[0][0][:], Data[1][0][:], Rrath, range = [[40,100],[0.65,1.05]])
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    U = np.dot(U,-1)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    V = D2[:,:,1]
    U = np.dot(U, 0)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    V = np.dot(V,-1)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    U = D1[:,:,0]
    V = D1[:,:,1]
    ax1.quiver (yy,xx, U, V)
    ax1.set_xlabel ('RH [norm.]')
    ax1.set_ylabel ('Pressure [norm.]')
    ax1.set_title ('2D Flow and variation + histogram')
    
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    #print (Data[0][0][:])
    #print (Data[1][0][:])
    plt.savefig ("var_RH_pres.png", dpi = 300)
    plt.show()
    plt.cla()

def WIND_RH(level = 4):
    time, RH = ta.read_time_data ("RHh.txt")
    time, windh = ta.read_time_data ("Windh.txt")
    RHs = plit_data (time, RH)
    wind = plit_data (time, windh)
    Data = []
    
    Data.append(RHs)
    Data.append(wind)
    #Data = md.Normalize_data(Data)
    #print (Data)
    #return Data
  
    D1, D2, H = md.multid3(Data, level = level, test = True)
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    for i in range(len(D1)):
        for j in range(len(D1[i])):
            if (H[i,j] < 50):
                for k in range(len(D1[i,j])):
                    D1[i,j,k] = 0
                    D2[i,j,k] = 0
    

    fig, (ax1) = plt.subplots(1, 1)
    ax1.pcolor (yy,xx, H, vmin = 0, vmax = 2*np.mean(np.mean(H)))
    U = D2[:,:,0]
    V = np.dot(U, 0)
    #plt.hist2d (Data[0][0][:], Data[1][0][:], Rrath, range = [[40,100],[0.65,1.05]])
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    U = np.dot(U,-1)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    V = D2[:,:,1]
    U = np.dot(U, 0)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    V = np.dot(V,-1)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    U = D1[:,:,0]
    V = D1[:,:,1]
    ax1.quiver (yy,xx, U, V)
    ax1.set_xlabel ('RH [norm.]')
    ax1.set_ylabel ('U [norm.]')
    ax1.set_title ('2D Flow and variation + histogram')
    
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    #print (Data[0][0][:])
    #print (Data[1][0][:])
    plt.savefig ("U_RH.png", dpi = 300)
    plt.show()
    plt.cla()

    
def rat_q(level = 4):
    time, rat = ta.read_time_data ("Rrath_era.txt")
    time, q = ta.read_time_data ("era_qh.txt")
    rat = [rat]
    q = [q]
    Data = []
    Data.append(rat)
    Data.append(q)
    print (len(Data))
    print (len(Data[0][0]))
    
    D1, D2, H = md.multid3(Data, level = level, test = True)
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    for i in range(len(D1)):
        for j in range(len(D1[i])):
            if (H[i,j] < 50):
                for k in range(len(D1[i,j])):
                    D1[i,j,k] = 0
                    D2[i,j,k] = 0
    

    fig, (ax1) = plt.subplots(1, 1)
    ax1.pcolor (yy,xx, H, vmin = 0, vmax = 2*np.mean(np.mean(H)))
    U = D2[:,:,0]
    V = np.dot(U, 0)
    #plt.hist2d (Data[0][0][:], Data[1][0][:], Rrath, range = [[40,100],[0.65,1.05]])
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    U = np.dot(U,-1)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    V = D2[:,:,1]
    U = np.dot(U, 0)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    V = np.dot(V,-1)
    ax1.quiver (yy, xx, U, V, color = 'magenta')
    U = D1[:,:,0]
    V = D1[:,:,1]
    ax1.quiver (yy,xx, U, V)
    ax1.set_xlabel ('LWD/LWU [norm.]')
    ax1.set_ylabel ('q [norm.]')
    ax1.set_title ('2D Flow and variation + histogram')
    
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    #print (Data[0][0][:])
    #print (Data[1][0][:])
    plt.savefig ("rat_q.png", dpi = 300)
    plt.show()
    plt.cla()

# Split a dim-by-time data set into dim-by-nrtime-by-time by splitting
# up the original data cutting away non-valid data (-9999)

def split_data (data, max_missing):
    start = 0
    end = 0
    maxi = 0
    counter = 0
    datan = []
    for dim in range(len(data)):
        datan.append([])
    tot = len(data[0])
    i = 0
    while i < tot:
        while i < tot:
            good_start = True
            for dim in range(len(data)):
                if (data[dim][i] == -9999):
                    good_start = False
            if good_start:
                start = i
                print (f'start = {start}')
                break ## Break out of inner while
            i += 1
            
        while i < tot:
            for dim in range(len(data)):
                increased_counter = False
                if data[dim][i] == -9999:
                    counter += 1
                    increase_counter = True
                    break # Break out of dim loop
                if not increased_counter:
                    counter = 0
            if (counter == max_missing):
                end = i - max_missing
                break # Break from the i-loop
            i += 1
        if (i == tot): # end of data series
            end = i
        for dim in range(len(data)):
            if (start < end):
                datan[dim].append(data[dim][start:end])
        i += 1
    print (f'there are {len(datan[0])} time series now')
    return datan
    
def use_md_dep (level = 3, endi = 10):
    time, rat = ta.read_time_data ("Rrath_era7200.txt")
    time, q = ta.read_time_data ("era_qh7200.txt")
    time, t = ta.read_time_data ("era_th7200.txt")
    time, u = ta.read_time_data ("era_uh7200.txt")
    time, v = ta.read_time_data ("era_vh7200.txt")
    time, w = ta.read_time_data ("era_wh7200.txt")
    U = [0]*len(u)
    for i in range(len(U)):
        U[i] = (u[i]**2 + v[i]**2)**0.5
    data = split_data ([q,t,U,rat], 10)
    for dim in range(len(data)):
        data[dim] = data[dim][0:endi]
    ratn = data.pop()
  
    Av, Var, H = md.dependency_check(ratn, data, level)
    
    print (f'There are on average {np.mean(H)} data points per cell')

    A = H*(H > max(np.mean(H)/4, 10))
    tot = np.mean(A)
    for val in np.arange(0, 1.5, 0.05):
        A = H*(H > max(np.mean(H)/4, 10))
        A = A*(Av > val)+A*(Av < -val)
        meanvar = np.sum(np.multiply(Var, A))/np.sum(A)
        print (f'{val}\t{np.mean(A)/tot}\t{meanvar}')

    
    return Av, Var, H

def era (level = 3, endi = 4):
    time, rat = ta.read_time_data ("Rrath_era3600.txt")
    time, q = ta.read_time_data ("era_qh3600.txt")
    time, t = ta.read_time_data ("era_th3600.txt")
    time, u = ta.read_time_data ("era_uh3600.txt")
    time, v = ta.read_time_data ("era_vh3600.txt")
    time, w = ta.read_time_data ("era_wh3600.txt")
    time, rh = ta.read_time_data ("era_rhh3600.txt")
    U = [0]*len(u)
    for i in range(len(U)):
        U[i] = (u[i]**2 + v[i]**2)**0.5
    var = rh
    name = 'rh'

    data = split_data ([rat,var], 1000)
    
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
    
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    U = D1[:,:,0]
    V = D1[:,:,1]
    if 1:
        fig, (ax1) = plt.subplots(1, 1)
        print ('hallo')
        ax1.quiver (yy,xx, U, V)
        ax1.set_xlabel ('LWD/LWU [norm.]')
        ax1.set_ylabel (name+' (900 hPa) [norm.]')
        plt.savefig(name+"era_rat.png", dpi = 200)
        
    fig, (ax1, ax2) = plt.subplots(1, 2)
    
    T, pe = md.residence_time (D1, D2)
    Trat = np.dot(T[:,:,0], 1./3600.)
    a = ax1.pcolor (yy,xx,Trat, vmin = 0, vmax = 13)
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Residence time [hours]')
    Perat = pe[:,:,0]
    a = ax2.pcolor (yy,xx, Perat, vmin = 0, vmax = 1)
    ax2.set_xlabel ('LWD/LWU [norm.]')
    ax1.set_ylabel (name+' [norm.]')
    ax2.set_aspect(1.0/ax2.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax2, location = 'bottom')
    b.set_label ('Diffusion-advection ratio')
    plt.savefig (name+'_era_T_pe.png', dpi = 200)

def era_3d (level = 3, endi = 2):
    time, rat = ta.read_time_data ("Rrath_era3600.txt")
    time, q = ta.read_time_data ("era_qh3600.txt")
    time, t = ta.read_time_data ("era_th3600.txt")
    time, u = ta.read_time_data ("era_uh3600.txt")
    time, v = ta.read_time_data ("era_vh3600.txt")
    time, w = ta.read_time_data ("era_wh3600.txt")
    time, RH = ta.read_time_data ("era_rhh3600.txt")
    U = [0]*len(u)
    for i in range(len(U)):
        U[i] = (u[i]**2 + v[i]**2)**0.5
        
    data = split_data ([rat, u, v], 1000)
    
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
    
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    N = 1 << level
    Qs = np.zeros([N, N])
    U = D1[:,:,:,0]
    if 1:
        return D1[:,:,:,0], D2[:,:,:,0], H[:,:,:]
    
    
    for i in range (N):
        for j in range (N):
            dqdt = U[:,i,j]
            W = H[:,i,j]
            if (np.sum (W) > 10):
                fit  = np.polyfit (xvals, dqdt, 1, full = True, w = W)
                f = np.dot (xvals, fit[0][0]) + fit[0][1]
                if (fit[0][0] < 0):
                    xzero = -fit[0][1]/fit[0][0]
                    if abs(xzero) < 1.5:
                        Qs[i,j] = xzero
                    else:
                        Qs[i,j] = np.nan
                else:
                    Qs[i,j] = np.nan
            else:
                Qs[i,j] = np.nan

   
                
    xx, yy = np.meshgrid (xvals, yvals)
    fig, (ax1) = plt.subplots(1, 1)
    a = ax1.pcolor (yy,xx, Qs , vmin = -1, vmax = 1)
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Stable point LWD/LWU [norm]')
    ax1.set_xlabel ('RH [norm.]')
    ax1.set_ylabel ('w [norm.]')
    
    plt.savefig ("StablepointRHw.png", dpi = 200)
    return Qs

def era_nv (level = 3, endi = 4):
    time, rat = ta.read_time_data ("Rrath_era3600.txt")
    time, q = ta.read_time_data ("era_qh3600.txt")
    time, t = ta.read_time_data ("era_th3600.txt")
    time, u = ta.read_time_data ("era_uh3600.txt")
    time, v = ta.read_time_data ("era_vh3600.txt")
    time, w = ta.read_time_data ("era_wh3600.txt")
    time, rh = ta.read_time_data ("era_rhh3600.txt")

    data = split_data ([rat, u, q, v, w], 1000)
    md.Normalize_data(data)
    
    for j in range(len(data[0])):
        for k in range(len(data[0][j])):
            data[1][j][k] += data[3][j][k] + data[2][j][k] - data[4][j][k]# u + v + q - w
    data.pop() # remove w
    data.pop() # remove v
    data.pop() # remove q
        
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)

    
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    U = D1[:,:,0]
    V = D1[:,:,1]
    if 1:
        fig, (ax1) = plt.subplots(1, 1)
        print ('hallo')
        ax1.quiver (yy,xx, U, V)
        ax1.set_xlabel ('LWD/LWU [norm.]')
        ax1.set_ylabel ('u+v+q-w (900 hPa) [norm.]')
        plt.savefig("uvwq_era_rat.png", dpi = 200)
        

    fig, (ax1, ax2) = plt.subplots(1, 2)
    
    T, pe = md.residence_time (D1, D2)
    Trat = np.dot(T[:,:,0], 1./3600.)
    a = ax1.pcolor (yy,xx,Trat, vmin = 0, vmax = 13)
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Residence time [hours]')
    Perat = pe[:,:,0]
    a = ax2.pcolor (yy,xx, Perat, vmin = 0, vmax = 1)
    ax2.set_xlabel ('LWD/LWU [norm.]')
    ax1.set_ylabel ('u+v+q-w [norm.]')
    ax2.set_aspect(1.0/ax2.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax2, location = 'bottom')
    b.set_label ('Diffusion-advection ratio')
    plt.savefig ('uvqw_era_T_pe.png', dpi = 200)
    
    
def era_3d_nv (level = 3, endi = 2):
    time, rat = ta.read_time_data ("Rrath_era3600.txt")
    time, q = ta.read_time_data ("era_qh3600.txt")
    time, t = ta.read_time_data ("era_th3600.txt")
    time, u = ta.read_time_data ("era_uh3600.txt")
    time, v = ta.read_time_data ("era_vh3600.txt")
    time, w = ta.read_time_data ("era_wh3600.txt")
    time, RH = ta.read_time_data ("era_rhh3600.txt")
  
    data = split_data ([rat, u, v, w, RH ], 1000)
    md.Normalize_data(data)

    if 0:
        for j in range(len(data[0])):
            for k in range(len(data[0][j])):
                data[1][j][k] += data[3][j][k] # u + v
                data[2][j][k] -= data[4][j][k] # q - w
            data.pop() # remove w
            data.pop() # remove v

    PC1 = [0.636, -0.635, 0.438] # u+v, w, rh
    PC2 = [-0.304, 0.314, 0.899]

    
    PC1 = [0.636, -0.524, 0.551] # u+v, w, q
    PC2 = [0.02, 0.725, 0.688]
    # PCA u+v, w, q
    if 1:
        for j in range(len(data[0])):
            for k in range(len(data[0][j])):
                u = data[1][j][k]
                data[1][j][k] += data[2][j][k] #u + v
                data[1][j][k] *= PC1[0]
                data[1][j][k] *= PC1[1]*data[3][j][k]
                data[1][j][k] *= PC1[2]*data[4][j][k]

                data[2][j][k] += u # u+v
                data[2][j][k] *= PC2[0]
                data[2][j][k] += PC2[1]*data[3][j][k]
                data[2][j][k] += PC2[2]*data[4][j][k]
        data.pop() # remove q
        data.pop() # remove w
    
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
    
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    N = 1 << level
    Qs = np.zeros([N, N])
    U = D1[:,:,:,0]
    for i in range (N):
        for j in range (N):
            dqdt = U[:,i,j]
            W = H[:,i,j]
            if (np.sum (W) > 10):
                fit  = np.polyfit (xvals, dqdt, 1, full = True, w = W)
                f = np.dot (xvals, fit[0][0]) + fit[0][1]
                if (fit[0][0] < 0):
                    xzero = -fit[0][1]/fit[0][0]
                    if abs(xzero) < 1.5:
                        Qs[i,j] = xzero
                    else:
                        Qs[i,j] = np.nan
                else:
                    Qs[i,j] = np.nan
            else:
                Qs[i,j] = np.nan
                
    xx, yy = np.meshgrid (xvals, yvals)
    fig, (ax1) = plt.subplots(1, 1)
    a = ax1.pcolor (yy,xx, Qs , vmin = -1, vmax = 1)
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Stable point LWD/LWU [norm]')
    ax1.set_xlabel ('1st PC [norm.]')
    ax1.set_ylabel ('2nd PC [norm.]')
    
    plt.savefig ("StablepointPCAwq.png", dpi = 200)
    return Qs


def era_residence (level = 3, endi = 2):
    time, rat = ta.read_time_data ("Rrath_era3600.txt")
    time, q = ta.read_time_data ("era_qh3600.txt")
    time, t = ta.read_time_data ("era_th3600.txt")
    time, u = ta.read_time_data ("era_uh3600.txt")
    time, v = ta.read_time_data ("era_vh3600.txt")
    time, w = ta.read_time_data ("era_wh3600.txt")
    data = split_data ([rat,q,t], 1000)
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
    T, pe = md.residence_time (D1, D2)
    
    return T, pe

def era_nv3 (level = 3, endi = 4):
    time, rat = ta.read_time_data ("Rrath_era3600.txt")
    time, q = ta.read_time_data ("era_qh3600.txt")
    time, t = ta.read_time_data ("era_th3600.txt")
    time, u = ta.read_time_data ("era_uh3600.txt")
    time, v = ta.read_time_data ("era_vh3600.txt")
    time, w = ta.read_time_data ("era_wh3600.txt")
    time, rh = ta.read_time_data ("era_rhh3600.txt")
    
    data = split_data ([rat, u, v, w, q], 1000)
    md.Normalize_data(data)

    PC1 = [0.636, -0.635, 0.438] # u+v, w, rh
    PC2 = [-0.304, 0.314, 0.899]

    PC1 = [0.636, -0.541, 0.5509] #u+v, w, q
    v = PC1
    if 1:
        for j in range(len(data[0])):
            for k in range(len(data[0][j])):
                u = data[1][j][k]
                data[1][j][k] += data[2][j][k] #u + v
                data[1][j][k] *= v[0]
                data[1][j][k] *= v[1]*data[3][j][k]
                data[1][j][k] *= v[2]*data[4][j][k]

                data[2][j][k] += u # u+v
                data[2][j][k] *= PC2[0]
                data[2][j][k] += PC2[1]*data[3][j][k]
                data[2][j][k] += PC2[2]*data[4][j][k]
                
                
        data.pop() # remove q
        data.pop() # remove w
        data.pop() # remove v
    md.Normalize_data(data)
    
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
    
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    U = D1[:,:,0]
    V = D1[:,:,1]
    if 1:
        fig, (ax1) = plt.subplots(1, 1)
        print ('hallo')
        ax1.quiver (yy,xx, U, V)
        ax1.set_xlabel ('LWD/LWU [norm.]')
        ax1.set_ylabel ('1st PC (900 hPa) [norm.]')
        plt.savefig("PC1_era_rat.png", dpi = 200)
        
        
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    
    T, pe = md.residence_time (D1, D2)
    Trat = np.dot(T[:,:,0], 1./3600.)
    a = ax1.pcolor (yy,xx,Trat, vmin = 0, vmax = 13)
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    ax1.set_ylabel ('1st PC [norm.]')
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Residence time [hours]')

    Perat = pe[:,:,0]
    a = ax2.pcolor (yy,xx, Perat, vmin = 0, vmax = 1)
    ax2.set_xlabel ('LWD/LWU [norm.]')
    ax2.set_aspect(1.0/ax2.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax2, location = 'bottom')
    b.set_label ('Diffusion-advection ratio')
    
    a = ax3.pcolor(yy, xx, D2[:,:,0], vmin = 0, vmax=5e-5)
    ax3.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax3, location = 'bottom')
    b.set_label ('Diffusion')
   
    plt.savefig ('PC_era_T_pe_D2.png', dpi = 200)


def era_3d_CCN (level = 3, endi = 2):
    time, rat = ta.read_time_data ("Rrath_era3600CCN.txt")
    time, q = ta.read_time_data ("era_qh3600CCN.txt")
    time, t = ta.read_time_data ("era_th3600CCN.txt")
    time, u = ta.read_time_data ("era_uh3600CCN.txt")
    time, v = ta.read_time_data ("era_vh3600CCN.txt")
    time, w = ta.read_time_data ("era_wh3600CCN.txt")
    time, RH = ta.read_time_data ("era_rhh3600CCN.txt")
    time, CCN = ta.read_time_data ("CCNh_era3600CCN.txt")
    U = [0]*len(u)
    for i in range(len(U)):
        U[i] = (u[i] + v[i])
        if (CCN[i] > 0):
            CCN[i] = np.log(CCN[i])
        

    data = split_data ([rat, CCN, U], 1000)
    
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
    
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    N = 1 << level
    Qs = np.zeros([N, N])
    U = D1[:,:,:,0]
    for i in range (N):
        for j in range (N):
            dqdt = U[:,i,j]
            W = H[:,i,j]
            if (np.sum (W) > 10):
                fit  = np.polyfit (xvals, dqdt, 1, full = True, w = W)
                f = np.dot (xvals, fit[0][0]) + fit[0][1]
                if (fit[0][0] < 0):
                    xzero = -fit[0][1]/fit[0][0]
                    if abs(xzero) < 1.5:
                        Qs[i,j] = xzero
                    else:
                        Qs[i,j] = np.nan
                else:
                    Qs[i,j] = np.nan
            else:
                Qs[i,j] = np.nan
                
    xx, yy = np.meshgrid (xvals, yvals)
    fig, (ax1) = plt.subplots(1, 1)
    a = ax1.pcolor (yy,xx, Qs , vmin = -1, vmax = 1)
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Stable point LWD/LWU [norm]')
    ax1.set_xlabel ('log(CCN) [norm.]')
    ax1.set_ylabel ('u + v [norm.]')
    plt.savefig ("StablepointCCNU.png", dpi = 200)

    plt.cla()
    b.remove()
    D2l = np.array (D2[:,:,:,0])
    D2l[D2l == 0] = np.nan
    D2lccnuv = np.nanmean(D2l, axis = 0)
    a = ax1.pcolor (yy,xx, D2lccnuv)
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Diffusion LWD/LWU [norm]')
    ax1.set_xlabel ('log(CCN) [norm.]')
    ax1.set_ylabel ('u + v [norm.]')
    plt.savefig ("DiffCCNU.png", dpi = 200)

    
    b.remove()
    D2l = np.array (D2[:,:,:,0])
    D2lv = np.nanstd(D2l, axis = 0)
    D2vrat = D2lv/D2lccnuv
    a = ax1.pcolor (yy,xx, D2vrat, vmin = 0, vmax = 2)
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Relative Variance [norm]')
    ax1.set_xlabel ('log(CCN) [norm.]')
    ax1.set_ylabel ('u + v [norm.]')
    plt.savefig ("varDiffCCNU.png", dpi = 200)

    b.remove()
    plt.cla()
    
    D1c = D1[:,5,2,0]
    D2c = D2[:,5,2,0]
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    a, = ax1.plot (xvals, D1c, label = 'Drift');
    b, = ax2.plot (xvals, D2c, 'r', label = 'Diffusion');
    
    plt.legend ([a,b], ["Drift", "Diffusion"])
    plt.title ('Example drift and diffusion')
    ax1.set_xlabel ('LWU/LWD [norm.]')
    plt.savefig ('Drift_diff_ccnuvbin.png', dpi = 200)
    plt.show()
        
        
    D2c = D1[:,4,5,0]
    D2c = D2[:,4,5,0]
    

def era_nv3_CCN (level = 3, endi = 4):
    time, rat = ta.read_time_data ("Rrath_era3600CCN.txt")
    time, q = ta.read_time_data ("era_qh3600CCN.txt")
    time, t = ta.read_time_data ("era_th3600CCN.txt")
    time, u = ta.read_time_data ("era_uh3600CCN.txt")
    time, v = ta.read_time_data ("era_vh3600CCN.txt")
    time, w = ta.read_time_data ("era_wh3600CCN.txt")
    time, RH = ta.read_time_data ("era_rhh3600CCN.txt")
    time, CCN = ta.read_time_data ("CCNh_era3600CCN.txt")

    for i in range(len(CCN)):
        if (CCN[i] > 0):
            CCN[i] = np.log(CCN[i])
            
    data = split_data ([rat, CCN, u, v, w, q], 1000)
    md.Normalize_data(data)

    PC1 = [0.636, -0.635, 0.438] # u+v, w, rh
    PC2 = [-0.304, 0.314, 0.899]

    PC1 = [0.636, -0.541, 0.5509] #u+v, w, q
    v = PC1
    if 1:
        for j in range(len(data[0])):
            for k in range(len(data[0][j])):
                u = data[1][j][k]
                data[2][j][k] += data[3][j][k] #u + v
                data[2][j][k] *= v[0]
                data[2][j][k] *= v[1]*data[4][j][k]
                data[2][j][k] *= v[2]*data[5][j][k]

                data[3][j][k] += u # u+v
                data[3][j][k] *= PC2[0]
                data[3][j][k] += PC2[1]*data[4][j][k]
                data[3][j][k] += PC2[2]*data[5][j][k]
                
                
        data.pop() # remove q
        data.pop() # remove w
        data.pop() # remove v
        data.pop() # remove PC2
    
    md.Normalize_data(data)
    
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
    
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    U = D1[:,:,0]
    V = D1[:,:,1]
    if 1:
        fig, (ax1) = plt.subplots(1, 1)
        print ('hallo')
        ax1.quiver (yy,xx, U, V)
        ax1.set_xlabel ('LWD/LWU [norm.]')
        ax1.set_ylabel ('1st PC (900 hPa) [norm.]')
        plt.savefig("PC1_era_rat.png", dpi = 200)
        
        
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
    
    T, pe = md.residence_time (D1, D2)
    Trat = np.dot(T[:,:,0], 1./3600.)
    a = ax1.pcolor (yy,xx,Trat, vmin = 0, vmax = 13)
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    ax1.set_ylabel ('log(CCN) [norm.]')
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Residence time [hours]')

    Perat = pe[:,:,0]
    a = ax2.pcolor (yy,xx, Perat, vmin = 0, vmax = 1)
    ax2.set_xlabel ('LWD/LWU [norm.]')
    ax2.set_aspect(1.0/ax2.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax2, location = 'bottom')
    b.set_label ('Diffusion-advection ratio')
    
    a = ax3.pcolor(yy, xx, D2[:,:,0], vmin = 0, vmax=5e-5)
    ax3.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax3, location = 'bottom')
    b.set_label ('Diffusion')
   
    plt.savefig ('CCN_era_T_pe_D2.png', dpi = 200)


def RAT_CCN(level = 4):
    time, rat = ta.read_time_data ("Rrath_era3600CCN.txt")
    time, CCN = ta.read_time_data ("CCNh_era3600CCN.txt")
    
    for i in range(len(CCN)):
        if CCN[i] > 0:
            CCN[i] = np.log(CCN[i])
            
    data = split_data ([rat, CCN], 1000)
    md.Normalize_data(data)

    
    D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
    dx = 4./(1 << level)
    xvals = np.array(np.arange(-2 + dx/2, 2, dx))
    yvals = np.array(np.arange(-2 + dx/2, 2, dx))
    xx, yy = np.meshgrid (xvals, yvals)
    for i in range(len(D1)):
        for j in range(len(D1[i])):
            if (H[i,j] < 50):
                for k in range(len(D1[i,j])):
                    D1[i,j,k] = 0
                    D2[i,j,k] = 0
    if 1:
        plt.cla()
        fig, (ax1) = plt.subplots(1, 1)
        ax1.pcolor (yy,xx, H, vmin = 0, vmax = 2*np.mean(np.mean(H)))
        U = D2[:,:,0]
        V = np.dot(U, 0)
        #plt.hist2d (Data[0][0][:], Data[1][0][:], Rrath, range = [[40,100],[0.65,1.05]])
        #ax1.quiver (yy, xx, U, V, color = 'magenta')
        U = np.dot(U,-1)
        #ax1.quiver (yy, xx, U, V, color = 'magenta')
        V = D2[:,:,1]
        U = np.dot(U, 0)
        #ax1.quiver (yy, xx, U, V, color = 'magenta')
        V = np.dot(V,-1)
        #ax1.quiver (yy, xx, U, V, color = 'magenta')
        U = D1[:,:,0]
        V = D1[:,:,1]
        ax1.quiver (yy,xx, U, V)
        ax1.set_xlabel ('CCN [norm.]')
        ax1.set_ylabel ('LWD/WLU [norm.]')
        ax1.set_title ('2D Flow and variation + histogram')
        
        ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
        plt.savefig ("var_CCN_rat.png", dpi = 300)
        plt.cla()
    T, Pe = md.residence_time (D1, D2, test = True)
   # return T, Pe, D1, D2
    fig, (ax1, ax2) = plt.subplots(1, 2)
    plt.title ('Statistics for LWD/WLU')
    Trat = np.dot(T[:,:,1], 1./3600.)
    a = ax1.pcolor (yy,xx,Trat, vmin = 0, vmax = 13)
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Residence time [hours]')
    Perat = Pe[:,:,1]
    a = ax2.pcolor (yy,xx, Perat, vmin = 0, vmax = 1)
    
    ax2.set_aspect(1.0/ax2.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax2, location = 'bottom')
    b.set_label ('Diffusion-advection ratio')
    plt.savefig('time_rat.png', dpi = 200)
    plt.cla()
    
    fig, (ax1, ax2) = plt.subplots(1, 2)
    plt.title ('Statistics for RH')
    Trat = np.dot(T[:,:,0], 1./3600.)
    a = ax1.pcolor (yy,xx,Trat, vmin = 0, vmax = 13)
    ax1.set_aspect(1.0/ax1.get_data_ratio(), adjustable='box')
    ax1.set_xlabel ('RH [norm.]')
    ax1.set_ylabel ('LWD/WLU [norm.]')
    b = plt.colorbar(a, ax = ax1, location = 'bottom')
    b.set_label ('Residence time [hours]')
    Perat = Pe[:,:,0]
    a = ax2.pcolor (yy,xx, Perat, vmin = 0, vmax = 1)
    ax2.set_xlabel ('CCN [norm.]')
    ax2.set_ylabel ('LWD/WLU [norm.]')
 
    ax2.set_aspect(1.0/ax2.get_data_ratio(), adjustable='box')
    b = plt.colorbar(a, ax = ax2, location = 'bottom')
    b.set_label ('Diffusion-advection ratio')
    plt.savefig ('time_CCN.png', dpi = 200)


def Rrat_1D (level = 5):
     time, rat = ta.read_time_data ("Rrath_era.txt")
     data = split_data ([rat], 1000)
     D1, D2, H = md.multid3(data, dt = 3600, level = level, test = True)
     D1 = np.array(D1)
     D1 = np.transpose(D1)
     D1 = D1[0]
     D1 = np.append (D1[0], D1)
     D1 = np.append (D1, D1[-1])
     a = [0.25, 0.5, 0.25]
     D1 = np.convolve (D1, a, 'valid')
     D2 = np.array(D2)
     D2 = np.transpose(D2)
     D2 = D2[0]
     D2 = np.append (D2[0], D2)
     D2 = np.append (D2, D2[-1])
     D2 = np.convolve (D2, a, 'valid')
     
     Dx = 4/(1 << level)
     x = np.arange (-2 + Dx/2., 2 , Dx)

     H = H/np.sum(H)
     dr.write_ascii (x, D1, "D1.txt")
     dr.write_ascii (x, D2, "D2.txt")
     dr.write_ascii (x, H, "pdf.txt")
     
     return D1, D2, H, x
 

 
