
#include "stdlib.h"
#include "math.h"
#include "stdio.h"
#include "time.h"
#define NP 1
#define HP 100

#define pi (3.14159265)
#define noise() (1. - 2.*rand()/(double)RAND_MAX)

#define TRANSFORM(x) (erf(1.2*x)+noise()/5.)
//#define TRANSFORM(x) (x > 0 ? log(1.5*x + 1) : -log(-1.5*x + 1))

double locs[NP], Tend = 10000;
int hist[HP];


void normal_sample (double z[2]) {
  double a[2] = {fabs(noise()), fabs(noise())};
  double r = sqrt(-2.*log(a[0]));
  z[0] = r*sin(a[1]*2.*pi);
  z[1] = r*cos(a[1]*2.*pi);
}

void hist_update (double x, double xl, double xr) {
  int index = HP*(x - xl)/(xr - xl);
  if (index >= 0 && index < HP)
    hist[index]++;
}

double drift_bi (double x) {
  return -5*x*(x - 1)*(x + 1);
}


double drift (double x) {
  return -x;
}


double diffusion (double x) {
  return 1;
}


void  advance_le (double dt) {
  for (int j = 0; j < NP; j++) {
    double n[2];
    normal_sample (n);
    locs[j] += dt*drift(locs[j]) + sqrt(dt*diffusion(locs[j]))*n[0];
  }
}


int main() {
  srand (time(NULL));
  double dt = Tend/9999999;
  double t = 0;
  for ( ; t <= Tend; t += dt) {
    advance_le (dt);
    //printf ("%g %g %g\n", t, locs[0], TRANSFORM(locs[0]));
    double z[2];
    normal_sample (z);
    hist_update (TRANSFORM(locs[0]), -1.2, 1.2);
  }
  double xl = -1.1, xr = 1.1, dx = (xr - xl)/HP;
  for (int i = 0; i < HP; i++) {
    fprintf (stderr, "%g %d\n", xl + (i + 0.5)*dx, hist[i]);
  }
}

