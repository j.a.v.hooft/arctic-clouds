import time_align as ta
import usemd as um
import numpy as np
import multid as md
import matplotlib.pyplot as plt
import data_reader as dr

def normalize_array(x):
    xn = x - [np.mean(x)]*len(x)
    xn = xn / [np.std(xn)]*len(x)
    return xn

def simple_PCA(data):
    C = np.cov (data)
    print (C)
    return np.linalg.eig(C)

def era():
    time, w = ta.read_time_data ("era_wh3600.txt")
    time, u = ta.read_time_data ("era_uh3600.txt")
    time, v = ta.read_time_data ("era_vh3600.txt")
    time, T = ta.read_time_data ("era_th3600.txt")
    time, q = ta.read_time_data ("era_qh3600.txt")
    time, rh = ta.read_time_data ("era_rhh3600.txt")
    uv = np.array(u) + np.array(v)
    datar = um.split_data ([u, v, w, q, T, rh, uv], 100)
    md.Normalize_data (datar)
    
    u1 = np.array(datar[0][0][:])
    v1 = np.array(datar[1][0][:])
    w1 = np.array(datar[2][0][:])
    q1 = np.array(datar[3][0][:])
    T1 = np.array(datar[4][0][:])
    rh1 = np.array(datar[5][0][:])
    uv1 = np.array(datar[6][0][:])
    print (len(u1), len (w1))
    for j in range (1, len(datar[0])):
        u1 = np.concatenate ((u1, np.array(datar[0][j][:])))
        v1 = np.concatenate ((v1, np.array(datar[1][j][:])))
        w1 = np.concatenate ((w1, np.array(datar[2][j][:])))
        q1 = np.concatenate ((q1, np.array(datar[3][j][:])))
        T1 = np.concatenate ((T1, np.array(datar[4][j][:])))
        rh1 = np.concatenate ((rh1, np.array(datar[5][j][:])))
        uv1 = np.concatenate ((uv1, np.array(datar[6][j][:])))
  
    print (len(uv1), len (rh1), len (u1),len (q1), len (T1), len(rh))
    
    print (u1[np.isnan(u1)])
    print (T1[np.isnan(w1)])

    
    data = [u1, v1, w1, q1]
    datan = [u, v,w,q]
    C = np.cov (data)
    w, v = np.linalg.eig(C)
    print (w)
    print (v)
    
    PC1 = [0]*len(u)
   
    pcv1 = v[:,0]
    PC2 = [0]*len(u)
    pcv2 = v[:,1]
    for i in range(len(u)):
        for j in range(len(data)):
            PC1[i] += pcv1[j]*datan[j][i]
            PC2[i] += pcv2[j]*datan[j][i]

        
    PC1 = np.matmul (pcv1, data)
    PC2 = np.matmul (pcv2, data)
    print(np.shape(PC1))
    print (len(uv1))
#    dr.write_ascii (time, PC1, "era_PC1")
 #   dr.write_ascii (time, PC2, "era_PC2")

    
    fig , ax = plt.subplots()
    n = 40
    dens = True
    print (v1)
    #ax.hist (u1, bins = n, density = dens, histtype = 'step', label = 'u')
    #ax.hist (v1, bins = n, density = dens, histtype = 'step', label = 'v')
    ax.hist (w1, bins = n, density = dens, histtype = 'step', label = 'w')
    #ax.hist (q1, bins = n, density = dens, histtype = 'step', label = 'q')
    #ax.hist (T1, bins = n, density = dens, histtype = 'step', label = 'T')
    ax.hist (rh1, bins = n, density = dens, histtype = 'step', label = 'rh')
    ax.hist (uv1, bins = n, density = dens, histtype = 'step', label = 'u + v')
    ax.hist (PC1, bins = n, density = dens, histtype = 'step', label = 'PC1')
    
    ax.set_xlim([-4, 4])
    ax.legend()
    fig.show()
    
    
    return v, w
    
    
    
    
    
    
