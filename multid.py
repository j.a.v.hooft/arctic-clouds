## Multidimensional Langevin analysis

## Data format
# The format of the `DATA` (list type) parameter is as follows.
# DATA[ix][iy][ik], where ix denotes the various variables, such that
# `len(DATA)` is the dimensionality of the parameter space. Then `iy`
# denotes the how-manieth section of a split timeseries. Note that
# this is "hard coded", meaning that if your data consists of one
# continuous series, you need to reshape it such that `len(DATA[0]) ==
# 1`, where `0` is an example index. Then `ik` denotes the
# home-manieth data entry in the particular section. such that
# `len(data[0][0])` is the duration of the first data section of the
# first variable.

# Note that the DATA matrix is a cuboid, where all data among the
# dimensions has the same shape in terms of sections and section
# lengths (and time interval). This may require some "alignment" if
# your data comes from different sources.

# A fill value is allowed but is hard-coded to be `-9999'

# You can see example usage in the `usemd.py' file

import numpy as np

# A function that normalizes data such that the mean is 0, and the
# standard deviation is 1.
def Normalize_data (DATA, test = False, tested = 0):
    # replace outliers
    Means = [0]*len(DATA)
    stds = [0]*len(DATA)
    Nr = [0]*len(DATA)
    for i in range(len(DATA)):
        for j in range(len(DATA[i])):
            for k in range(len(DATA[i][j])):
                if (DATA[i][j][k] <= -9999):
                    DATA[i][j][k] = np.NaN
    for i in range(len(DATA)):
        for j in range(len(DATA[i])):
            for val in DATA[i][j]:
                if val > -9999:
                    Nr[i] += 1
    if test:
        print (f'Nrs = {Nr}')
    for i in range(len(DATA)):
        for j in range(len(DATA[i])):
           for val in DATA[i][j]:
                if (val > -9999):
                    Means[i] += val
        Means[i] /= Nr[i]
    for i in range(len(DATA)):
        for j in range(len(DATA[i])):
            for val in DATA[i][j]:
                if (val > -9990):
                    stds[i] += (val - Means[i])**2
        stds[i] /= Nr[i]
        stds[i] = stds[i]**0.5
        for j in range(len(DATA[i])):
            for k in range(len(DATA[i][j])):
                if (DATA[i][j][k] > -9999):
                    DATA[i][j][k] -= Means[i]
                    if stds[i] > 0:
                        DATA[i][j][k] /= stds[i]
                    
    if test:
        print (f'Means: {Means}')
        print (f'stds: {stds}')
        if (tested == 0):
            Normalize_data (DATA, test, 1)
    return DATA, Means, stds


# A helper function that computes the index of a cell as a function of (?)
def cell_index (cell, R, test = False):
    cellnr = cell
    DIM = len(R)
    indarr = [-9999]*DIM
    counter = 0
    while (counter < DIM):
        indarr[DIM-counter - 1] = int(np.floor(cell/R[DIM - counter - 1]))
        cell -= indarr[DIM - counter - 1]*R[DIM - counter - 1]
        counter += 1
    #if (test):
     #   print (f'cell: {cellnr}, index: {tuple(indarr)}')
    return tuple((indarr))

# Get indices of Data entries in the cell indicates by `ind`
def indics (Data, ind, level, sigma):
    dx = 2*sigma/(1 << level)
    indices = []
    for j in range(len(Data[0])):
        for k in range(len(Data[0][j])):
            include = [0]*len(Data)
            for i in range(len(Data)):
                if Data[i][j][k] > -sigma + ind[i]*dx and Data[i][j][k] <= -sigma + (ind[i] + 1)*dx:
                    include[i] = 1
            if (np.sum(include) == len(Data)):
                indices.append([j,k])
    return indices

# A function that computes the mean and varance of the increments at `t = t(0) + lookat` 
def mean_inc (Data, indices, lookat, test):
    Means_inc = np.zeros([len(Data), len(lookat)])
    Vars_inc = np.zeros([len(Data), len(lookat)])
    Nen = np.zeros([len(Data), len(lookat)])
    for i in range(len(lookat)):
        for index in indices:
            for dim in range(len(Data)):
                baseval = Data[dim][index[0]][index[1]]
                if (baseval > -9999 and index[1] + lookat[i] < len(Data[dim][index[0]]) and index[1] + lookat[i] >= 0): 
                    if (Data[dim][index[0]][index[1] + lookat[i]] > -9999):
                        Means_inc[dim][i] += Data[dim][index[0]][index[1] + lookat[i]] - baseval
                        Nen[dim][i] += 1
        for dim in range(len(Data)):
            if Nen[dim][i] > 0:
                Means_inc[dim][i] /= Nen[dim][i]
    if (test):
        print (f'Mi = {Means_inc}')
    for i in range(len(lookat)):
        for index in indices:
            for dim in range(len(Data)):
                baseval = Data[dim][index[0]][index[1]]
                if (baseval > -9999 and index[1] + lookat[i] < len(Data[dim][index[0]]) and index[1] + lookat[i] >= 0): #look-at index exists 
                    if (Data[dim][index[0]][index[1] + lookat[i]] > -9999):
                        Vars_inc[dim][i] += ((Data[dim][index[0]][index[1] + lookat[i]] - baseval) - Means_inc[dim][i])**2
        for dim in range(len(Data)):
            if Nen[dim][i] > 0:
                Vars_inc[dim][i] /= Nen[dim][i]
    if (test):
        print (f'Vi = {Vars_inc}')
   
    return Means_inc, Vars_inc, Nen

def D1_fit (incrs, weigth, lookat, dt, test):
    D1 = [0]*len(incrs)
    D1e = [0]*len(incrs)
    t = np.dot(lookat, dt)
    for dim in range(len(incrs)):
        if test:
            print (f'incrs  = {incrs[dim]}')
            print (f' w = {weigth[dim]}')
        if (np.sum(np.fabs(incrs[dim])) > 0): # There should be some data
            fit  = np.polyfit (t, incrs[dim], 1, full = True, w = weigth[dim])
            if fit[2] > 1: # OK 
                D1[dim] = fit[0][0]
                D1e[dim] = fit[1][0]
            else:
                D1e[dim] = np.NaN
        else:
            D1e[dim] = np.NaN
    return D1

def D2_fit (incrs, weigth, lookat, dt, test):
    D2 = [0]*len(incrs)
    D2e = [0]*len(incrs)
    t = np.dot(lookat, dt)
    for dim in range(len(incrs)):
        if test:
            print (f'var_incrs  = {incrs[dim]}')
            print (f' w = {weigth[dim]}')
        if (np.sum(np.fabs(incrs[dim])) > 0): # There should be some data
            fit  = np.polyfit (t, incrs[dim], 1, full = True, w = weigth[dim])
            if fit[2] > 1: # OK 
                D2[dim] = fit[0][0]
                D2e[dim] = fit[1][0]
            else:
                D2e[dim] = np.NaN
        else:
            D2e[dim] = np.NaN
    return D2




# This function returns the conditional dift and diffusion coefficient
# for a normalized dataset. The conditions is set by the (hyper)
# cubic range specified by the index, and the domain size ([-sig, sig]^DIM) 

def drift_diffuse_N (Data, dt, ind, level, lookat = range(0,5), sigma = 2, test = False):
    # Setup
    D1 = [0]*len(Data)
    D2 = [0]*len(Data)
    N = 0
    # Get the indices in the time series that fit the cell
    indices = indics (Data, ind, level, sigma)
    
    N = len(indices)
    
    if (N > 0):
        Mi, Vi, Ni = mean_inc(Data, indices, lookat, test)
        D1 = D1_fit (Mi, Ni, lookat, dt, test)
        D2 = D2_fit (Vi, Ni, lookat, dt, test)
    return D1, D2, N
    

# return the N-dimensional drift and diffusion vector with a histogram
# The first index of these N dimensional arrays correspond to the
# first variable in the data list.  This data list consists of
# segmented timeseries of equal length, e.g:

# Drows = [[a_1(t) = [1,2,3],[7,8]], a_2(t) = [[4,5,6], [5,4]]]


def multid3 (DRows, dt = 1, level = 6, test = False):
    NrR = len(DRows)
    if (test):
        print (f'A {NrR}-dimensional parameter space')
    N = 1 << level
    DRows, means, stds = Normalize_data (DRows, test)
    D = [N]
    L = [N]
    R = [1]
    for i in range(NrR - 1):
        D.append (N)
        L.append (N)
        R.append (pow(N,i + 1))
    L.append (NrR)
    D1 = np.zeros (L)
    D2 = np.zeros (L)
    H = np.zeros (D)
    for cell in range(pow(N, NrR)): 
        i = cell_index (cell, R, test)
        D1c, D2c, H[i] = drift_diffuse_N (DRows, dt, i, level, test = test)
        for dim in range(len(DRows)):
            il = list(i)
            il.append(dim)
            i = tuple(il)
            D1[i] = D1c[dim]
            D2[i] = D2c[dim]
            il.pop()
            i = tuple(il)
    return D1, D2, H

## Some utilities for analysis

# Peclet : TD/TA = sigma**2/D2 / sigma/D1 = sigmaD1/D2
def residence_time (D1, D2, travel_Distance = 1., test = False):
    N = D1.shape[0]
    NrR = D1.shape[-1] 
    T = np.ones(D1.shape)
    T = np.dot(T, np.NaN)
    Pe = np.ones(D1.shape)
    Pe = np.dot(Pe, np.NaN)
    R = [1]
    for i in range(D1.shape[-1] - 1):
        R.append (pow(N, i + 1))
    for cell in range(pow(N, NrR)): 
        i = cell_index (cell, R)
        il = list(i)
        for dim in range(NrR):
            il.append(dim)
            i = tuple(il)
            il.pop()
            Per = np.NaN
            TD = np.NaN
            TA = np.NaN
            
            if np.fabs(D2[i]) > 0: 
                TD = travel_Distance**2/D2[i]
            if np.fabs(D1[i]) > 0:
                TA = travel_Distance/np.fabs(D1[i])
            print (f"{TA} {TD}")
            if (np.fabs(TD + TA) > 0):
                Per = TA/(TD + TA)
            Pe[i] = Per
            if (TA > 0 and TD > 0):
                T[i] = 1/(1/TA + 1/TD)
            elif (TA > 0):
                T[i] = TA
            elif (TD > 0):
                T[i] = TD
            else:
                T[i] = np.NaN
            if test:
                print (f'D1 = {D1[i]}, D2 = {D2[i]}, T= {T[i]}, Pe = {Pe[i]}')
    return T, Pe

def average_std_cell (Data, indicex):
    avg = 0
    std = 0
    n = 0
    for index in indicex:
        if Data[index[0]][index[1]] > -9999:
            avg += Data[index[0]][index[1]]
            n += 1
    if (n > 0):
        avg /= n
    for index in indicex:
        if Data[index[0]][index[1]] > -9999:
            std += (Data[index[0]][index[1]] - avg)**2
    if (n > 0):
        std /= n
    if (std > 0):
        std = std**0.5
    return avg, std, n

def dependency_check (Data1, depends_on, level = 4, sigma = 2, test = False):
    Data1, av1, std1 = Normalize_data([Data1])
    depends_on, av2, std2 = Normalize_data(depends_on)
    NrR = len(depends_on)
    print (NrR)
    N = 1 << level
    print (f'Your parmeter space is discretized with {N**NrR} cells.')
    D = [N]
    L = [N]
    R = [1]
    for i in range(NrR - 1):
        D.append (N)
        L.append (N)
        R.append (pow(N,i + 1))
    Av = np.zeros (D)
    Var = np.zeros (D)
    H = np.zeros (D)
    for cell in range(pow(N, NrR)): 
        i = cell_index (cell, R, test)
        indices = indics (depends_on, i, level, sigma)
        A, V, N = average_std_cell (Data1[0], indices)
        H[i] = N
        Av[i] = A
        Var[i] = V
    return Av, Var, H

