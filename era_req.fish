#!/usr/bin/env fish

set years $argv

for year in $years
    nohup ./era_reques.py $year >> reqdata 2>&1 &
end
