 double rval = -10;
 
void compute_rhos (face vector D1, scalar D2, scalar rho) {
  rval = 10;
  double drv = 0.1;
  while (1) {
    //printf ("%g %g\n", rval, drv);
    rho[left] = -rval;
    boundary ({rho});
    foreach() {
      rho[] = rho[-1] + Delta*4*D1.x[]/(D2[-1] + D2[]) -
	(log(D2[]) - log(D2[-1]));
    }
    double total = 0, minval = HUGE;
    foreach(reduction(+:total) reduction(min:minval)) {
      rho[] = exp(rho[]);
      total += rho[]*Delta;
    }
    if (total < 1)
      rval -= drv;
    else {
      rval += rval;
      drv /= 4;
    }
    if (fabs(total - 1) < 1e-3) {
      rho[left] = neumann(0);
      return;
    }
  }
}

void find_D1 (scalar rhos, scalar D2, face vector D1) {
  boundary ({rhos, D2});
  foreach_face () {
    D1.x[] = 0.5*(D2[] + D2[-1])/2*((log(D2[]) - log(D2[-1]))/Delta +
				    (log(rhos[0]) - log(rhos[-1]))/Delta);
    D1.x[] = max(D1.x[], -0.005);
    D1.x[] = min(D1.x[], 0.005);
    
  }
}

scalar D1c[], D2r[], rhosg;
double dl = -9;
#include "solve.h"
void find_D2 (scalar rhos, scalar D2, face vector D1) {
  //scalar  D1c[], D2r[];
  rhosg = rhos;
  foreach() {
    D1c[] = (D1.x[1] + D1.x[])/2.;
    D2[] = log(D2[]);
    D2r[] = D2[];
  }
  
  double dp = -9, dpp = 9, stp = .5;
  dl = -20;
  for (int j = 0; j < 50; j++) {
    D2[left] = dl;
    boundary ({D2});
    foreach() {
      D2[] =  (D2[-1] + Delta*(2*D1c[]/exp(D2[-1]) -
			       (log(rhos[]) - log(rhos[-1]))/(Delta)));
      dp = D2[];
    }
    if (j == 0)
      dpp = dp + 1;
    if (dp < dpp) {
      dl += stp;
      dpp = dp;
    }
    else {
      dl -= 1.5*stp;
      stp /= 2;
      dpp = dp + 1;
    }
    printf ("%d %g %g\n",j, dl, dp);
  }
  foreach() {
    //printf ("%g %g %g\n", x, D2[],  exp(D2[]));
    D2[] = exp(min(D2[], -9));// + D2[])/2);
  }
  D2[left]= neumann(0);
}
