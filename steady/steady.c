#include "grid/multigrid1D.h"
#define RKORDER 2
#include "fpe.h"
#include "loader.h"
#include "steady.h"
rho[left] = 0.; 
rho[right] = 0.;

scalar rhos[], rhom[];

void normalize_pdf (scalar s) {
  double tot = 0;
  foreach(reduction(+:tot))
    tot += Delta*s[];
  foreach()
    s[] /= tot;
  tot = 0;
  foreach(reduction(+:tot))
    tot += Delta*s[];
}



int main() {
  L0 = 5;//0.55;
  X0 = -L0/2.;//0.60;
  DT = 5;
  N = 128;
  DT = 10;
  run();
}

event init (t = 0) {
  scalar D1s[];
  load_scalar ("D1.txt", D1s);
  foreach_face()
    D1.x[] = face_value (D1s, 0);
  load_scalar ("D2.txt", D2);
  load_scalar ("pdf.txt", rhom);
  normalize_pdf (rhom);
  compute_rhos (D1, D2, rhos);
  
  face vector D1n[], D1nn[];
  scalar rhoi[], D2n[], D2nn[];
  foreach() {
    D2n[] = D2nn[] =  1e-7;
    if (x < -2.)
      rhom[] = 0.04*exp((x+2)*10);
    if (x > -2 && x < -2 + 1*Delta)
      rhom[] = (rhom[-1] + rhom[1])/2.;
    rhoi[] = (rhos[] + rhom[])/2;
    fflush (stdout);
  }
  find_D2 (rhoi, D2n, D1);
  //return 1;
  find_D1 (rhom, D2n, D1n);

  find_D1 (rhoi, D2, D1nn);
  find_D2 (rhom, D2nn, D1nn);
  
  foreach() {
    rho[] = exp(-sq(x - 1))/sqrt(pi);
    fprintf (stderr, "%g %g %g %g %g %g %g %g %g\n", x,
	     rhos[], (D1.x[] + D1.x[1])/2., D2[],
	     rhom[], (D1n.x[] + D1n.x[1])/2., D2n[],
	     (D1nn.x[] + D1nn.x[1])/2., D2nn[]);
  }
  return 1;
}

event stop (t = 3600*24*2) {
  foreach()
    printf ("%g %g %g\n", x, rho[], rhom[]);
}

