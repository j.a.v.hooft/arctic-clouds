/**
 */

//#include "grid/cartesian1D.h"
#include "grid/multigrid1D.h"
#define RKORDER 2
#include "fpe.h"

#define NP 1000
double locs[NP];
// No drift flux:
rho[left] = 0.; 
rho[right] = 0.;

// Box-Muller transform

void normal_sample (double z[2]) {
  double a[2] = {fabs(noise()), fabs(noise())};
  double r = sqrt(-2.*log(a[0]));
  z[0] = r*sin(a[1]*2.*pi);
  z[1] = r*cos(a[1]*2.*pi);
}

void make_pdf (scalar pdf) {
  for (int j = 0; j < NP; j++) {
    Point point = locate (locs[j]);
    if (point.level > 0)
      pdf[] += 1./(NP*Delta);
  }
}

void print_pdfs (FILE * fp) {
  scalar pdf[];
  make_pdf (pdf);
  foreach() {
    printf ("%g %g %g\n", x, pdf[], rho[]);
  }
}

double interp_array (double xp, int n, float xa[n], float arr[n]) {
  float dx = ((xa[5] - xa[0]))/5;
  //printf ("data: %g %f %f %f %f %g\n", xp, xa[0], xa[1], xa[2], xa[3], dx);
  if (xp <= xa[0])
    return arr[0];
  if (xp >= xa[n - 1])
    return arr[n-1];
  double ind = (xp - xa[0])/dx;
  //printf ("ind %g\n", ind);
  int indi = (int)ind;
  ind = fmod (ind, 1);
  if (indi >= n - 1 || indi < 0) {
    fprintf (stderr, "Index error\n");
    exit (1);
  }
  if (ind >= 0 && ind < 1)
    return (ind*arr[indi + 1] + (1 - ind)*arr[indi]);
  else
    return 0;
}

void load_D1_D2 (char * fname, face vector D1, scalar D2) {
  FILE * fp = fopen (fname, "r");
  if (fp == NULL)
    fprintf (stderr, "error opening file\n");
  float x, D1e, D2e;
  int lines = 0;
  while (!feof(fp)) {
    if (fscanf (fp, "%f %f %f\n", &x, &D1e, &D2e) == 3)
      lines++;
  }
  float xa[lines], D1a[lines], D2a[lines];
  lines = 0;
  rewind (fp);
  while (!feof(fp)) {
    fscanf (fp, "%f %f %f\n", &xa[lines], &D1a[lines], &D2a[lines]);
    lines++;
  }
  foreach() {
    D2[] = 2*interp_array (x, lines, xa, D2a);
  }
  foreach_face() {
    D1.x[] = interp_array (x, lines, xa, D1a);
  }
}

int main() {
  L0 = 0.55;
  X0 = 0.60;
  DT = 5;
  N = 128;
  run();
}

event init (t = 0) {
  load_D1_D2 ("D1D2.txt", D1, D2);
  scalar D1c[];
  scalar rhos[];
  foreach() {
    D1c[] = (D1.x[] + D1.x[1])/2;
    rhos[] = exp(-sq((x - 0.88)/(0.04)));
    rho[] = exp(-sq((x - 0.77)/(0.02)));
  }
  stats m = statsf(rho);
  foreach() {
    rho[] /= m.sum;
    rhos[] /= m.sum;
    if (rhos[] < 0)
      rhos[] = 0;
  }
  for (int j = 0; j < NP; j++)
    locs[j] = 0.95;
  //compute_D2 (D1c, rhos, D2);
  //foreach()
  //  printf ("%g %g %g %g\n ",x, D2[], D1c[], b[]);
  //return 1;
}

scalar rhon[]; 

event output (t = {3600, 36000, 72000}) {
  char fname[99];
  sprintf (fname, "data1_%g", t);
  FILE * fp = fopen (fname, "w");
  foreach()
    fprintf (fp,"%g %g\n", (x-0.87)/0.1 ,rho[]);
  fclose (fp);
}

event stop (i += 5) {
  if (change (rho, rhon) < 1e-6) {
    stats m = statsf (rho);
    printf ("# %g %g\n", t, m.sum);
    event ("finalize_movie");
    
    print_pdfs (stdout);
    return 1;
  }
}

double interval = 600;
event logger (t += interval) {
  char fname[99];
  sprintf (fname, "data_fpe_right_%g", interval);
  static FILE * fp = fopen (fname, "w");
  if (i == 0) {
    foreach()
      fprintf (fp, "%g ", x);
    fputc ('\n', fp);
  }
  foreach()
    fprintf (fp, "%g ", rho[]);
  fputc ('\n', fp);
}

event too_long (t = 48*3600){
  event ("finalize_movie");
  FILE * fp = fopen ("fin.dat", "w");
  for (int j = 0; j < NP; j++) {
    fprintf (fp, "%g\n", locs[j]);
  }
  print_pdfs (stdout);
    
  return 1;
  
}

event advance_le (i++) {
  for (int j = 0; j < NP; j++) {
    double n[2];
    normal_sample (n);
    Point point = locate (locs[j]);
    if (point.level > 0)
      locs[j] += dt*interpolate(D1.x, locs[j]) + sqrt(dt*interpolate (D2, locs[j]))*n[0];
    else
      locs[j] = X0 + L0/2.;
  }
}

event trajectories (t = 12*3600; t += 3600) {
  static FILE * fp = fopen ("le_trajs", "w");
  for (int j = 0; j < NP; j++) {
    fprintf (fp, "%g\t", locs[j]);
  }
  fputc ('\n', fp);
}

/**
## Movie maker code

`Gnuplot` and `FFmpeg` are used to create a movie from the solution
data.
 */

FILE * gnuplotPipe;

event init (t = 0) {
  gnuplotPipe = popen ("gnuplot", "w");
  fprintf(gnuplotPipe,
	  "set term pngcairo\n"
	  "set xr [0.65: 1.05]\n"
	  "set yr [0: 10]\n"
	  "set key box top left\n"
	  "set grid\n"
	  //"set title 'PDF evolution'\n"
	  "set xlabel 'x'\n"
	  "set ylabel 'pdf'\n");
}

int frame = 0;
event movie(i += 100){
  fprintf(gnuplotPipe,
	  "set output 'plot%d.png'\n"
	  "set title 'time = %01.3g hours\n",
	  frame, t/3600);
  fprintf(gnuplotPipe, "plot \
          '-' w l lw 5 t 'pdf',			\
          '-' w l lw 5 t 'ppdf',		\
          '-' w l lw 2 lc 'black' t 'Drift', \
          '-' w l lw 2 lc 'black' lt ':' t 'Diffusion'\n");
  scalar pdf[];
  make_pdf (pdf);
  for (scalar s in {rho, pdf, D1.x, D2}) {
    foreach()
      fprintf(gnuplotPipe, "%g %g\n",x, s[]);
    fprintf(gnuplotPipe, "e\n");
  }
  frame++;
}

event finalize_movie(0) {
  system("rm mov.mp4");
  system("ffmpeg -r 25 -f image2 -i plot%d.png -c:v libx264 -vf format=yuv420p -y mov.mp4");
  system("rm plot*");
}

